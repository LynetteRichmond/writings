The Art of Writing
==========

Lines and shape changes the beauty of art. As I am searching the [emergencyessay](https://emergencyessay.com) for a school project, I stumble upon in an exquisite art of Calligraphy that is created in different version from different country. 


The example below is made from the stroke of culture and design as one of the major styles in art and science. You can check the sample and examine every single output of each stroke.



![Screenshot of art](https://i.ytimg.com/vi/Y-G7tMUH76A/maxresdefault.jpg)

![Screenshot of art](https://blogtmssl-templatemonster.netdna-ssl.com/wp-content/uploads/2014/03/40-Free-Calligraphy-Fonts.jpg?7a9951)

![Screenshot of art](http://www.calligraphy-skills.com/images/laws-of-nib1e.gif)
